import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Rodape from './Componentes/Rodape';
import Cabecalho from './Componentes/Cabecalho';
import Inicio from './Componentes/Inicio';
import Formulario from './Componentes/Formulario';
import Apoio from './Componentes/Apoio';
import Contato from './Componentes/Contato';



class App extends Component {
  render(){
    return (
      <div className="App">
        <BrowserRouter>
         <Cabecalho /> 

         <Route exact path="/"  component={Inicio}/>
         <Route path="/Formulario" component={Formulario}/> 
         <Route path="/Apoio" component={Apoio}/>
         <Route path="/Contato" component={Contato}/>
         <Rodape />
      
       </BrowserRouter>
      </div>
    );
  } 
}
export default App;