import React from 'react';

class Formulario extends React.Component {

  constructor(props) {
    super(props);
    this.state = {nome: '',
                 sobrenome: '',
                  email: '',
                  senha: '',
                  data: '',
                  cpf: '',
                  sexo:'',
                  estado:''

  
  };

    this.alterarNome = this.alterarNome.bind(this);
    this.alteraSobrenome =this.alterarSobrenome.bind(this);
    this.alterarEmail= this.alterarEmail.bind(this);
    this.alterarSenha = this.alterarSenha.bind(this);
    this.alterarData = this.alterarData.bind(this);
    this.alterarCpf = this.alterarCpf.bind(this);
    this.alteraSexo= this.alteraSexo.bind(this);
    this.alteraEstado= this.alteraEstado.bind(this);
     this.mostrarDados = this.mostrarDados.bind(this);
    
  }

  alterarNome = e => {
    
    this.setState({nome: e.target.value});
  }
  alterarSobrenome= e => {
    
    this.setState({sobrenome: e.target.value});
  }

  alterarEmail = e => {
    this.setState({email: e.target.value});
  }

  alterarSenha = e => {
    this.setState({senha: e.target.value});
  }
  alterarData = e => {
    this.setState({data: e.target.value});
  }

  alterarCpf= e => {
    this.setState({cpf: e.target.value});
  }

  alteraSexo= e => {
    this.setState({sexo: e.target.value});

  }

  alteraEstado= e => {
    this.setState({estado: e.target.value});

  }

 


    mostrarDados = (event) => {
     alert('Nome: ' + this.state.nome +  ' | Sobrenome: ' + this.state.sobrenome +' | Email: ' + this.state.email + ' | CPF: ' + this.state.cpf +
   ' | Data de nascimento: ' + this.state.data +' | Senha: ' + this.state.senha + ' | Sexo: ' + this.state.sexo + ' | Estado: ' + this.state.estado);
      event.preventDefault();
   }

 

    render(){

        return (
       <div className='text-center container-fluid bg-grey'>

         <h1>PREENCHA O FORMULÁRIO DE INSCRIÇÃO</h1>
         <p></p>
         
        <form  onSubmit={this.mostrarDados} >

       <div className='Nome'>
          <label> 
          <h4>Nome: </h4>
              <input type='text'  className='form-control' name='nome' placeholder=' Digite o seu nome!' required nome={this.state.nome} onChange={this.alterarNome}/>
              <h4>Sobrenome: </h4>
              <input type='text'  className='form-control' name='nome' placeholder=' Digite o seu sobrenome!' required sobrenome={this.state.sobrenome} onChange={this.alterarSobrenome}/>
          </label >
          </div>

          <div className='email'>
            <label>
            <h4>E-mail: </h4>
              <input type='email'  className='form-control' name='email' placeholder= 'Por exemplo mauro@email.com' required email={this.state.email} onChange={this.alterarEmail}/>
            </label>
          </div>


           <div className='CPF'>
            <label>
            <h4>CPF: </h4>
             <input type='text'  className='form-control' name='CPF' placeholder='Por exemplo: 000.000.000-00' required cpf={this.state.cpf} onChange={this.alterarCpf}/>
            </label>
            </div>
      
            <div className='senha'>
           <label>
           <h4>Senha: </h4>
                <input type='password' className='form-control' name='senha' placeholder=' Digite a sua senha' required senha={this.state.senha} onChange={this.alterarSenha}/>&nbsp;
             </label>
              </div>

              <div className='data'>
            <label>
            <h4> Data de Nascimento: </h4>
            <input type='date' name='data'  className='form-control'   required data={this.state.data} onChange={this.alterarData}/>
            </label>
            </div>
              <div className="sexo">
            <label> <h4>Sexo: </h4> </label>
            &nbsp;<select sexo={this.state.sexo} onChange={this.alteraSexo} name="sexo" required>
            <option value="">Selecione</option> 
             <option value="Masculino">Masculino</option> 
             <option value="Feminino">Feminino</option>  
             <option value="Prefiro não responder">Prefiro não responder</option>  
              </select>
              </div>

              <div>
            <label> <h4>Estado: </h4></label>
            &nbsp;<select estado={this.state.estado} onChange={this.alteraEstado} name="Estados" required>
            <option value="">Selecione</option> 
            <option value="AC">Acre</option> 
            <option value="AL">Alagoas</option> 
            <option value="AM">Amazonas</option> 
            <option value="AP">Amapá</option> 
            <option value="BA">Bahia</option> 
            <option value="CE">Ceará</option> 
            <option value="DF">Distrito Federal</option> 
            <option value="ES">Espírito Santo</option> 
            <option value="GO">Goiás</option> 
            <option value="MA">Maranhão</option> 
            <option value="MT">Mato Grosso</option> 
            <option value="MS">Mato Grosso do Sul</option> 
            <option value="MG">Minas Gerais</option> 
            <option value="PA">Pará</option> 
            <option value="PB">Paraíba</option> 
            <option value="PR">Paraná</option> 
            <option value="PE">Pernambuco</option> 
            <option value="PI">Piauí</option> 
            <option value="RJ">Rio de Janeiro</option> 
            <option value="RN">Rio Grande do Norte</option> 
            <option value="RO">Rondônia</option> 
            <option value="RS">Rio Grande do Sul</option> 
            <option value="RR">Roraima</option> 
            <option value="SC">Santa Catarina</option> 
            <option value="SE">Sergipe</option> 
            <option value="SP">São Paulo</option> 
            <option value="TO">Tocantins</option> 
          </select>
        </div>

            
            <div className= 'enviar'>
              <br/>
            <input type='submit' value='Enviar Formulário'/>
            </div>
          </form>
            </div>      
     )
   }

  }


export default Formulario;